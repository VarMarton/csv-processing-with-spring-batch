CREATE TABLE IF NOT EXISTS "listing_status"
(
    id         BIGSERIAL NOT NULL
    CONSTRAINT listing_status_pk PRIMARY KEY,
    status_name TEXT      NOT NULL
);