CREATE TABLE IF NOT EXISTS "marketplace"
(
    id         BIGSERIAL NOT NULL
    CONSTRAINT marketplace_pk PRIMARY KEY,
    marketplace_name TEXT      NOT NULL
);