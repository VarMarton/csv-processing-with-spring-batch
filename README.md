# csv-processing-with-spring-batch

---

### What is the purpose of this application?

This app was created to practise Spring Boot Batch alongside with Maven

---

### Requirements

* *Java 11* 
* *Docker*
* *docker-compose*

---

###Running application on local machine 

Start the database on your local machine and then you simply will able to debug / run the code.

Start docker-compose to init database

    docker-compose up -d

If you want to stop and purge the database

    docker-compose down -v

If you simply want to stop the database

    docker-compose down

**IMPORTANT**

Before running make sure *person*, *person_2*, *listing_status* and *marketplace* tables are created