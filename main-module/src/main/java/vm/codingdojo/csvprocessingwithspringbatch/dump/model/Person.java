package vm.codingdojo.csvprocessingwithspringbatch.dump.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Person {
	private long id;
	private String firstName;
	private String lastName;
	private int age;
}
