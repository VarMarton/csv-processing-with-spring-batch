package vm.codingdojo.csvprocessingwithspringbatch.personstore.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vm.codingdojo.csvprocessingwithspringbatch.model.Person;

@Configuration
public class NameAppenderStepConfiguration {
	
	private final String STEP_NAME = "nameAppenderStep";
	
	private final int SKIP_LIMIT = 5;
	private final int CHUNK_SIZE = 10;
	
	private StepBuilderFactory stepBuilderFactory;
	private FlatFileItemReader<Person> itemReader;
	private ItemProcessor<Person, Person> itemProcessor;
	private JdbcBatchItemWriter<Person> itemWriter;
	
	public NameAppenderStepConfiguration(StepBuilderFactory stepBuilderFactory,
										 FlatFileItemReader<Person> personItemReader,
										 ItemProcessor<Person, Person> itemProcessor,
										 JdbcBatchItemWriter<Person> itemWriter) {
		this.stepBuilderFactory = stepBuilderFactory;
		this.itemReader = personItemReader;
		this.itemProcessor = itemProcessor;
		this.itemWriter = itemWriter;
	}
	
	@Bean
	public Step nameAppenderStep() {
		return stepBuilderFactory
				.get(STEP_NAME)
				.<Person, Person>chunk(CHUNK_SIZE)
				.reader(itemReader)
				.readerIsTransactionalQueue()
				.processor(itemProcessor)
				.writer(itemWriter)
				.faultTolerant()
				.build();
	}
	
}
