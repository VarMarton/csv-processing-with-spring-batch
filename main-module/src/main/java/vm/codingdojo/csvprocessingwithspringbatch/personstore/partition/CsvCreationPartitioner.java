package vm.codingdojo.csvprocessingwithspringbatch.personstore.partition;

import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CsvCreationPartitioner implements Partitioner {
	
	@Value("${input.table}")
	private String sourceTable;
	@Value("${output.file}")
	private String outputFileLocation;
	
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		Map<String, ExecutionContext> map = new HashMap<>(gridSize);
		int i = 0, k = 1;
		for (Resource resource : resources) {
			ExecutionContext context = new ExecutionContext();
			context.putString(keyName, resource.getFilename());
			context.putString("opFileName", "output"+k+++".xml");
			map.put(PARTITION_KEY + i, context);
			i++;
		}
		return map;
	}
	
	@Bean
	public CsvCreationPartitioner partitioner() {
		return new CsvCreationPartitioner();
	}
}
