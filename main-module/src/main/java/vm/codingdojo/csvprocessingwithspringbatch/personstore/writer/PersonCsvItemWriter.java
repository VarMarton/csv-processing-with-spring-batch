package vm.codingdojo.csvprocessingwithspringbatch.personstore.writer;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import vm.codingdojo.csvprocessingwithspringbatch.model.Person;

@Configuration
public class PersonCsvItemWriter {
	
	private final boolean SHOULD_APPEND = false;
	
	@Bean
	@StepScope
	public FlatFileItemWriter<Person> csvItemWriter(@Value("#{stepExecutionContext[opFileName]}") String outputFileLocation) {
		return new FlatFileItemWriterBuilder<Person>()
				.name("personCsvItemWriter")
				.resource(new FileSystemResource(outputFileLocation))
				.append(SHOULD_APPEND)
				.encoding("utf8")
				.lineAggregator(new DelimitedLineAggregator<>() {
					{
						setDelimiter(DelimitedLineTokenizer.DELIMITER_TAB);
						setFieldExtractor(new BeanWrapperFieldExtractor<>() {
							{
								setNames(new String[]{"lastName", "firstName", "age"});
							}
						});
					}
				})
				.build();
	}
}
