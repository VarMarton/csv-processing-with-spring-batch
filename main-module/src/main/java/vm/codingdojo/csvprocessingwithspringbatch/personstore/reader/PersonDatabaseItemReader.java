package vm.codingdojo.csvprocessingwithspringbatch.personstore.reader;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.builder.JdbcPagingItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import vm.codingdojo.csvprocessingwithspringbatch.model.Person;

import javax.sql.DataSource;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class PersonDatabaseItemReader {
	
	private final boolean SAVE_STATE = false;
	private final int FETCH_SIZE = 100;
	private final int PAGE_SIZE = 100;
	
	@Bean
	@StepScope
	public JdbcPagingItemReader<Person> databaseItemReader(DataSource dataSource, @Value("#{stepExecutionContext[opFileName]}") String sourceTable) {
		return new JdbcPagingItemReaderBuilder<Person>()
				.name("databaseItemReader")
				.rowMapper(getRowMapper())
				.selectClause(getSelectClause())
				.fromClause(sourceTable)
				.sortKeys(getSortKey())
				.fetchSize(FETCH_SIZE)
				.pageSize(PAGE_SIZE)
				.saveState(SAVE_STATE)
				.dataSource(dataSource)
				.build();
	}
	
	private RowMapper<Person> getRowMapper() {
		return BeanPropertyRowMapper.newInstance(Person.class);
	}
	
	private String getSelectClause() {
		return "id, first_name AS firstName, last_name AS lastName, age";
	}
	
	private Map<String, Order> getSortKey() {
		Map<String, Order> sortKeys = new LinkedHashMap<>();
		sortKeys.put("id", Order.ASCENDING);
		return sortKeys;
	}
}
