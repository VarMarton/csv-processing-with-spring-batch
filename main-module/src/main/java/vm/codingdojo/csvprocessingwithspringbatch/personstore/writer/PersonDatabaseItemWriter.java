package vm.codingdojo.csvprocessingwithspringbatch.personstore.writer;

import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import vm.codingdojo.csvprocessingwithspringbatch.model.Person;

import javax.sql.DataSource;

@Configuration
public class PersonDatabaseItemWriter {
	
	@Bean
	public JdbcBatchItemWriter<Person> databaseItemWriter(final DataSource dataSource, @Value("${output.table}") String targetTable) {
		JdbcBatchItemWriter<Person> writer = new JdbcBatchItemWriter<>();
		writer.setDataSource(dataSource);
		writer.setSql(getInsertIntoPersonSql(targetTable));
		writer.setItemSqlParameterSourceProvider(person -> {
			final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
			sqlParameterSource.addValue("firstname", person.getFirstName()).
					addValue("lastName", person.getLastName())
					.addValue("age", person.getAge());
			return sqlParameterSource;
		});
		writer.afterPropertiesSet();
		return writer;
	}
	
	private String getInsertIntoPersonSql(String targetTable) {
		return "INSERT INTO " + targetTable + " (first_name, last_name, age) VALUES (:firstname, :lastName, :age)";
	}
}
