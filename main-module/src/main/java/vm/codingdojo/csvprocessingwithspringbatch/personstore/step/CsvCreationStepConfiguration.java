package vm.codingdojo.csvprocessingwithspringbatch.personstore.step;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vm.codingdojo.csvprocessingwithspringbatch.model.Person;

@RequiredArgsConstructor
@Configuration
public class CsvCreationStepConfiguration {
	
	private final int SKIP_LIMIT = 5;
	private final int CHUNK_SIZE = 10;
	
	private StepBuilderFactory stepBuilderFactory;
	private JdbcPagingItemReader<Person> itemReader;
	private FlatFileItemWriter<Person> itemWriter;
	
	@Bean
	public Step csvCreationStep() {
		return stepBuilderFactory
				.get("csvCreationStep")
				.<Person, Person>chunk(CHUNK_SIZE)
				.reader(itemReader)
				.readerIsTransactionalQueue()
				.writer(itemWriter)
				.faultTolerant()
				.build();
	}
}
