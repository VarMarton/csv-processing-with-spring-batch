package vm.codingdojo.csvprocessingwithspringbatch.personstore.reader;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import vm.codingdojo.csvprocessingwithspringbatch.model.Person;

@Configuration
public class PersonCsvItemReader {
	
	@Value("${input.file}")
	private String inputFileLocation;
	
	@Bean
	public FlatFileItemReader<Person> csvItemReader() {
		return new FlatFileItemReaderBuilder<Person>()
				.name("csvItemReader")
				.resource(new FileSystemResource(inputFileLocation))
				.encoding("utf8")
				.lineMapper(lineMapper())
				.build();
	}
	
	@Bean
	public DefaultLineMapper<Person> lineMapper(){
		DefaultLineMapper<Person> lineMapper = new DefaultLineMapper<>();
		lineMapper.setLineTokenizer(lineTokenizer());
		lineMapper.setFieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {
			{
				setTargetType(Person.class);
			}
		});
		return lineMapper;
	}
	
	@Bean
	public DelimitedLineTokenizer lineTokenizer() {
		DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer(DelimitedLineTokenizer.DELIMITER_TAB);
		tokenizer.setNames("lastName", "firstName", "age");
		return tokenizer;
	}
	
}
