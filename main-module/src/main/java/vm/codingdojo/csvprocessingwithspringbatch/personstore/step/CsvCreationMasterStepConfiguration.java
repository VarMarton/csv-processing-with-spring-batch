package vm.codingdojo.csvprocessingwithspringbatch.personstore.step;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vm.codingdojo.csvprocessingwithspringbatch.model.Person;
import vm.codingdojo.csvprocessingwithspringbatch.personstore.partition.CsvCreationPartitioner;

@RequiredArgsConstructor
@Configuration
public class CsvCreationMasterStepConfiguration {
	
	private final int SKIP_LIMIT = 5;
	private final int CHUNK_SIZE = 10;
	
	private StepBuilderFactory stepBuilderFactory;
	private CsvCreationPartitioner partitioner;
	
	@Bean
	public Step csvCreationMasterStep() {
		return stepBuilderFactory
				.get("csvCreationMasterStep")
				.partitioner("csvCreationStep",partitioner)
				.build();
	}
}
