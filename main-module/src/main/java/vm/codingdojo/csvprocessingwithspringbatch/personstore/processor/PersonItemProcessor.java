package vm.codingdojo.csvprocessingwithspringbatch.personstore.processor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.annotation.Configuration;
import vm.codingdojo.csvprocessingwithspringbatch.model.Person;

@Slf4j
@Configuration
public class PersonItemProcessor implements ItemProcessor<Person, Person> {
	
	
	@Override
	public Person process(Person person) {
		String newLastName = person.getLastName() + "_test";
		
		log.debug("Old last name: " + person.getLastName() + ", new one: " + newLastName);
		
		person.setLastName(newLastName);
		
		return person;
	}
}
