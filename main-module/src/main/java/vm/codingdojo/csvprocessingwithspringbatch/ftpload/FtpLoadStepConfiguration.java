package vm.codingdojo.csvprocessingwithspringbatch.ftpload;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class FtpLoadStepConfiguration {
	
	private final StepBuilderFactory stepBuilderFactory;
	private final FtpLoadTasklet ftpLoadTasklet;
	
	@Bean
	public Step ftpLoadStep() {
		return stepBuilderFactory
				.get("ftpLoadStep")
				.tasklet(ftpLoadTasklet)
				.build();
	}
}
