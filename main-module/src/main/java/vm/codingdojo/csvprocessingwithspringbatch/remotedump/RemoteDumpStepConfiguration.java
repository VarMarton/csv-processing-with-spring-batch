package vm.codingdojo.csvprocessingwithspringbatch.remotedump;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vm.codingdojo.csvprocessingwithspringbatch.dump.model.Person;


@Slf4j
@Configuration
@RequiredArgsConstructor
public class RemoteDumpStepConfiguration {
	
	private final StepBuilderFactory stepBuilderFactory;
	private final FlatFileItemWriter<Person> remoteDumpWriter;
	private final JdbcPagingItemReader<Person> remoteDumpReader;
	
	@Bean
	public Step remoteDumpStep() {
		return stepBuilderFactory
				.get("remoteDumpStep")
				.<Person, Person>chunk(100)
				.faultTolerant()
				.skipLimit(3)
				.skip(Exception.class)
				.reader(remoteDumpReader)
				.writer(remoteDumpWriter)
				.build();
	}
	
}