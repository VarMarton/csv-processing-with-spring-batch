package vm.codingdojo.csvprocessingwithspringbatch.remoteload.repository;

import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.postgresql.copy.CopyManager;
import org.postgresql.jdbc.PgConnection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.*;

@Slf4j
@Repository
@RequiredArgsConstructor
public class RemoteLoadRepository {
	
	@Value("${remote.file}")
	private String fileName;
	@Value("${input.table}")
	private String table;
	private final HikariDataSource dataSource;
	
	public void loadDumpIntoTable() {
		try {
			new CopyManager(dataSource.getConnection().unwrap(PgConnection.class))
					.copyIn("COPY " + table + " FROM STDIN DELIMITER '\t' CSV", new FileReader(fileName));
		} catch (Exception throwables) {
			log.error(throwables.getMessage());
		}
	}
	
}
