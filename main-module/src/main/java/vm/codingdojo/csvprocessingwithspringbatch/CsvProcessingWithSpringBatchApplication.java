package vm.codingdojo.csvprocessingwithspringbatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import vm.codingdojo.common.database.RemoteDataSourceConfiguration;

@SpringBootApplication(scanBasePackageClasses = {JobConfiguration.class, RemoteDataSourceConfiguration.class})
public class CsvProcessingWithSpringBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsvProcessingWithSpringBatchApplication.class, args);
	}

}
