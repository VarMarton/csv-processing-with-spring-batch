package vm.codingdojo.csvprocessingwithspringbatch.ftputil;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("ftp")
@Data
public class FtpClientProperties {
	private String host;
	private Integer port;
	private String username;
	private String password;
	private String filePath;
}
