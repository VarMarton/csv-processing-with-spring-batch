package vm.codingdojo.csvprocessingwithspringbatch;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.job.flow.support.SimpleFlow;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vm.codingdojo.common.common.CustomExitStatus;

@Slf4j
@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
public class JobConfiguration {
	
	private final JobBuilderFactory jobBuilderFactory;
	private final Step remoteDumpStep;
	private final Step remoteLoadStep;
	private final Step csvCreationMasterStep;
	private final Step nameAppenderStep;
	private final Step processListingStatusFromApiStep;
	private final Step processMarketplaceFromApiStep;
	private final Step ftpLoadStep;
	
	@Bean
	protected Job csvProcessingJob() {
		log.debug("Creating job");
		return jobBuilderFactory
				.get("batchDojoJob")
				.incrementer(new RunIdIncrementer())
				.start(remoteDumpStep)
				.next(remoteLoadStep)
				.on(CustomExitStatus.FTP.getExitCode()).to(ftpLoadStep).next(afterDecision()).from(remoteLoadStep)
				.on(ExitStatus.COMPLETED.getExitCode()).to(afterDecision()).from(remoteLoadStep)
				.end()
				.build();
	}
	
	private Flow afterDecision() {
		return new FlowBuilder<SimpleFlow>("smart-ftp-flow")
				.start(csvCreationMasterStep)
				.next(nameAppenderStep)
				.next(processMarketplaceFromApiStep)
				.next(processListingStatusFromApiStep)
				.end();
	}
	
}
