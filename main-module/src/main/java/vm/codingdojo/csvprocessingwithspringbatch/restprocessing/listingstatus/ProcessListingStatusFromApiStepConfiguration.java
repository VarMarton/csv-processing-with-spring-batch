package vm.codingdojo.csvprocessingwithspringbatch.restprocessing.listingstatus;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vm.codingdojo.csvprocessingwithspringbatch.model.ListingStatus;

@Configuration
@RequiredArgsConstructor
public class ProcessListingStatusFromApiStepConfiguration {
	
	private final StepBuilderFactory stepBuilderFactory;
	private final ItemReader<ListingStatus> listingStatusItemReader;
	private final JdbcBatchItemWriter<ListingStatus> listingStatusDatabaseItemWriter;
	
	@Bean
	public Step processListingStatusFromApiStep() {
		return stepBuilderFactory
				.get("processListingStatusFromApiStep")
				.<ListingStatus, ListingStatus>chunk(100)
				.faultTolerant()
				.skipLimit(3)
				.skip(Exception.class)
				.reader(listingStatusItemReader)
				.writer(listingStatusDatabaseItemWriter)
				.build();
	}
}
