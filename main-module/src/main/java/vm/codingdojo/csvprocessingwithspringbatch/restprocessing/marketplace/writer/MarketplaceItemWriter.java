package vm.codingdojo.csvprocessingwithspringbatch.restprocessing.marketplace.writer;

import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import vm.codingdojo.csvprocessingwithspringbatch.model.Marketplace;

import javax.sql.DataSource;

@Configuration
public class MarketplaceItemWriter {
	
	@Value("${rest.marketplace-table}")
	private String marketplaceTable;
	
	@Bean
	public JdbcBatchItemWriter<Marketplace> marketplaceDatabaseItemWriter(final DataSource dataSource) {
		JdbcBatchItemWriter<Marketplace> writer = new JdbcBatchItemWriter<>();
		writer.setDataSource(dataSource);
		writer.setSql(getInsertIntoMarketplaceSql());
		writer.setItemSqlParameterSourceProvider(marketplace -> {
			final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
			sqlParameterSource.addValue("id", marketplace.getId()).
					addValue("marketplaceName", marketplace.getMarketplaceName());
			return sqlParameterSource;
		});
		writer.afterPropertiesSet();
		return writer;
	}
	
	private String getInsertIntoMarketplaceSql() {
		return "INSERT INTO " + marketplaceTable + " (id, marketplace_name) VALUES (:id, :marketplaceName)";
	}
}
