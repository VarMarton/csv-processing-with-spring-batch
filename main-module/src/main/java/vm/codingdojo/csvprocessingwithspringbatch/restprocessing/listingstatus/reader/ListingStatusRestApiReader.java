package vm.codingdojo.csvprocessingwithspringbatch.restprocessing.listingstatus.reader;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import vm.codingdojo.csvprocessingwithspringbatch.model.ListingStatus;

import java.util.Arrays;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Configuration
public class ListingStatusRestApiReader implements ItemReader<ListingStatus> {
	
	@Value("${rest.listing-status-url}")
	private String apiUrl;
	private final RestTemplate restTemplate;
	
	private int nextListingStatusIndex;
	private List<ListingStatus> listingStatuses;
	
	@Bean
	public ItemReader<ListingStatus> listingStatusItemReader(RestTemplate restTemplate) {
		return new ListingStatusRestApiReader(restTemplate);
	}
	
	@Override
	public ListingStatus read() {
		if (listingStatusIsNotInitialized()) {
			this.listingStatuses = fetchListingStatus();
		}
		
		ListingStatus listingStatus = null;
		
		if (nextListingStatusIndex < listingStatuses.size()) {
			listingStatus = listingStatuses.get(nextListingStatusIndex);
			nextListingStatusIndex++;
		} else {
			nextListingStatusIndex = 0;
			listingStatuses = null;
		}
		return listingStatus;
	}
	
	private boolean listingStatusIsNotInitialized() {
		return this.listingStatuses == null;
	}
	
	private List<ListingStatus> fetchListingStatus() {
		ResponseEntity<ListingStatus[]> response = restTemplate.getForEntity(apiUrl,
				ListingStatus[].class
		);
		ListingStatus[] listingStatuses = response.getBody();
		log.debug("Download listing statuses from:");
		log.debug(apiUrl);
		log.debug("Number of downloaded listing statuses:");
		log.debug(String.valueOf(listingStatuses.length));
		return Arrays.asList(listingStatuses);
	}
}
