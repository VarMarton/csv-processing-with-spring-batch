package vm.codingdojo.csvprocessingwithspringbatch.restprocessing.marketplace;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vm.codingdojo.csvprocessingwithspringbatch.model.Marketplace;

@Configuration
@RequiredArgsConstructor
public class ProcessMarketplaceFromApiStepConfiguration {
	
	private final StepBuilderFactory stepBuilderFactory;
	private final ItemReader<Marketplace> marketplaceItemReader;
	private final JdbcBatchItemWriter<Marketplace> marketplaceDatabaseItemWriter;
	
	@Bean
	public Step processMarketplaceFromApiStep() {
		return stepBuilderFactory
				.get("processMarketplaceFromApiStep")
				.<Marketplace, Marketplace>chunk(100)
				.faultTolerant()
				.skipLimit(3)
				.skip(Exception.class)
				.reader(marketplaceItemReader)
				.writer(marketplaceDatabaseItemWriter)
				.build();
	}
}
