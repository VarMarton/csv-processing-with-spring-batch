package vm.codingdojo.csvprocessingwithspringbatch.restprocessing.listingstatus.writer;

import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import vm.codingdojo.csvprocessingwithspringbatch.model.ListingStatus;

import javax.sql.DataSource;

@Configuration
public class ListingStatusItemWriter {
	
	@Value("${rest.listing-status-table}")
	private String listingStatusTable;
	
	@Bean
	public JdbcBatchItemWriter<ListingStatus> listingStatusDatabaseItemWriter(final DataSource dataSource) {
		JdbcBatchItemWriter<ListingStatus> writer = new JdbcBatchItemWriter<>();
		writer.setDataSource(dataSource);
		writer.setSql(getInsertIntoListingStatusSql());
		writer.setItemSqlParameterSourceProvider(listingStatus -> {
			final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
			sqlParameterSource.addValue("id", listingStatus.getId()).
					addValue("statusName", listingStatus.getStatusName());
			return sqlParameterSource;
		});
		writer.afterPropertiesSet();
		return writer;
	}
	
	private String getInsertIntoListingStatusSql() {
		return "INSERT INTO " + listingStatusTable + " (id, status_name) VALUES (:id, :statusName)";
	}
}
