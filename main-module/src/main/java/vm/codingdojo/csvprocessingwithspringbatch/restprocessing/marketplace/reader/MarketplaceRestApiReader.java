package vm.codingdojo.csvprocessingwithspringbatch.restprocessing.marketplace.reader;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import vm.codingdojo.csvprocessingwithspringbatch.model.Marketplace;

import java.util.Arrays;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Configuration
public class MarketplaceRestApiReader implements ItemReader<Marketplace> {
	
	@Value("${rest.marketplace-url}")
	private String apiUrl;
	private final RestTemplate restTemplate;
	
	private int nextMarketplaceIndex;
	private List<Marketplace> marketplaces;
	
	@Bean
	public ItemReader<Marketplace> marketplaceItemReader(RestTemplate restTemplate) {
		return new MarketplaceRestApiReader(restTemplate);
	}
	
	@Override
	public Marketplace read() {
		if (marketplacesIsNotInitialized()) {
			this.marketplaces = fetchMarketplaces();
		}
		
		Marketplace marketplace = null;
		
		if (nextMarketplaceIndex < marketplaces.size()) {
			marketplace = marketplaces.get(nextMarketplaceIndex);
			nextMarketplaceIndex++;
		} else {
			nextMarketplaceIndex = 0;
			marketplaces = null;
		}
		return marketplace;
	}
	
	private boolean marketplacesIsNotInitialized() {
		return this.marketplaces == null;
	}
	
	private List<Marketplace> fetchMarketplaces() {
		ResponseEntity<Marketplace[]> response = restTemplate.getForEntity(apiUrl,
				Marketplace[].class
		);
		Marketplace[] marketplaces = response.getBody();
		log.debug("Download marketplaces from:");
		log.debug(apiUrl);
		log.debug("Number of downloaded marketplaces:");
		log.debug(String.valueOf(marketplaces.length));
		return Arrays.asList(marketplaces);
	}
}
