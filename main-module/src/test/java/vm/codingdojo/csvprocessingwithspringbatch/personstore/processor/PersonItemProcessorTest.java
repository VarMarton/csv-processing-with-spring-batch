package vm.codingdojo.csvprocessingwithspringbatch.personstore.processor;

import org.junit.Assert;
import org.junit.Test;
import vm.codingdojo.csvprocessingwithspringbatch.model.Person;

public class PersonItemProcessorTest {
	
	@Test
	public void processTest () {
		PersonItemProcessor processor = new PersonItemProcessor();
		
		Person person = new Person();
		person.setLastName("Doe");
		person.setFirstName("John");
		person.setAge(33);
		
		processor.process(person);
		
		Assert.assertEquals("Doe_test", person.getLastName());
		Assert.assertEquals("John", person.getFirstName());
		Assert.assertEquals(33, person.getAge());
	}
}
