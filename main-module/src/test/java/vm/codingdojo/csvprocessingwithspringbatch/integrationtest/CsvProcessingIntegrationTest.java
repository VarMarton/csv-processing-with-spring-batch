package vm.codingdojo.csvprocessingwithspringbatch.integrationtest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.RunWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.item.util.FileUtils;
import org.springframework.batch.test.AssertFile;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import vm.codingdojo.csvprocessingwithspringbatch.CsvProcessingWithSpringBatchApplication;
import vm.codingdojo.csvprocessingwithspringbatch.integrationtest.repository.CsvProcessingTestRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@EnableAutoConfiguration
@SpringBatchTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CsvProcessingWithSpringBatchApplication.class}, initializers = ConfigDataApplicationContextInitializer.class)
public class CsvProcessingIntegrationTest {
	
	@Autowired
	private JobLauncherTestUtils jobLauncherTestUtils;
	@Autowired
	private CsvProcessingTestRepository repository;
	
	@Value("${remote.file}")
	private String testFilePath;
	@Value("${remote.expected-file}")
	private String expectedFilePath;
	@Value("${input.table}")
	private String inputTable;
	
	@Test
	public void testRemoteDumpStep() throws Exception {
		JobExecution jobExecution = jobLauncherTestUtils.launchStep("remoteDumpStep");
		Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
		
		File testFile = new File(testFilePath);
		File expectedFile = new File(expectedFilePath);
		AssertFile.assertFileEquals(testFile, expectedFile);
		
		Files.delete(Path.of(testFilePath));
	}
	
	@Test
	public void testRemoteLoadStep() throws IOException {
		Files.copy(Path.of(expectedFilePath), Path.of(testFilePath));
		
		JobExecution jobExecution = jobLauncherTestUtils.launchStep("remoteLoadStep");
		Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
		
		//Assert.assertEquals(Integer.valueOf(1), repository.countPersonInTable());
		
		Files.delete(Path.of(testFilePath));
	}
	
	private void testCsvCreationStep() {
		JobExecution jobExecution = jobLauncherTestUtils.launchStep("csvCreationStep");
		Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
	}
	
	private void testNameAppenderStep() {
		JobExecution jobExecution = jobLauncherTestUtils.launchStep("nameAppenderStep");
		Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
	}
	
	private void testProcessListingStatusFromApiStep() {
		JobExecution jobExecution = jobLauncherTestUtils.launchStep("processListingStatusFromApiStep");
		Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
	}
	
	private void testProcessMarketplaceFromApiStep() {
		JobExecution jobExecution = jobLauncherTestUtils.launchStep("processMarketplaceFromApiStep");
		Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
	}
}
