package vm.codingdojo.csvprocessingwithspringbatch.integrationtest.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import vm.codingdojo.csvprocessingwithspringbatch.model.ListingStatus;
import vm.codingdojo.csvprocessingwithspringbatch.model.Marketplace;
import vm.codingdojo.csvprocessingwithspringbatch.model.Person;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class CsvProcessingTestRepository {
	
	private final NamedParameterJdbcTemplate jdbcTemplate;
	
	public Integer countPersonInTable(String tableName, String firstName, String lastName) {
		return jdbcTemplate.queryForObject(
				"select count(*) from " + tableName + " where first_name = ':firstName' and last_name = ':lastName'",
				new MapSqlParameterSource("firstName", firstName)
						.addValue("lastName", lastName),
				new BeanPropertyRowMapper<>(Integer.class)
		);
	}
}
