CREATE TABLE IF NOT EXISTS "test_person"
(
    id         BIGSERIAL NOT NULL
    CONSTRAINT test_person_id_pk PRIMARY KEY,
    first_name TEXT      NOT NULL,
    last_name  TEXT      NOT NULL,
    age        integer   NOT NULL
);

INSERT INTO "test_person" (id, first_name, last_name, age)
VALUES (1, 'Elek', 'Teszt', 30),
       (2, 'Réka', 'Nyom', 21),
       (3, 'Zita', 'Para', 32);